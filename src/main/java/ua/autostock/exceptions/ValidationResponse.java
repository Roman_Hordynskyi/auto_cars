package ua.autostock.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.validation.BindingResult;
import ua.autostock.dto.Exception.ValidationError;

import java.util.ArrayList;
import java.util.List;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor


public class ValidationResponse {

    private List<ValidationError> errors;
    private String error;
    private int status;

    public ValidationResponse(BindingResult errors, String error, int status) {
        this.errors = new ArrayList<ValidationError>();
        errors.getFieldErrors().stream().forEach(err ->  this.errors.add(new ValidationError(err.getField(), err.getDefaultMessage())));
        errors.getGlobalErrors().stream().forEach(err ->  this.errors.add(new ValidationError(err.getObjectName(), err.getDefaultMessage())));
        this.error = error;
        this.status = status;
    }

}
