package ua.autostock.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.autostock.repository.FilterRepo.MakeRepository;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class CheckUniqueFilterMakeValidator implements ConstraintValidator<CheckUniqueFilterMake, String> {

    @Autowired
    private MakeRepository makeRepository;

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if(makeRepository.findByMake(value) == null) {
            return true;
        }
        return false;
    }
}
