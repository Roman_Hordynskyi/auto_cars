package ua.autostock.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.autostock.repository.FilterRepo.ModelRepository;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class CheckUniqueFilterModelValidator implements ConstraintValidator<CheckUniqueFilterModel, String> {
    @Autowired
    private ModelRepository modelRepository;

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if(modelRepository.findByModel(value)== null)
            return true;
        return false;
    }
}
