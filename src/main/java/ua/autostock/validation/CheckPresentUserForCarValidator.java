package ua.autostock.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.autostock.dto.UserDTO;
import ua.autostock.repository.UserRepository;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class CheckPresentUserForCarValidator implements ConstraintValidator<CheckPresentUserForCar, UserDTO> {

    @Autowired
    private UserRepository userRepository;

    @Override
    public boolean isValid(UserDTO value, ConstraintValidatorContext context) {
        if (value.getId() == null)
            return false;
        if (userRepository.findById(value.getId()).isPresent())
            return true;
        return false;
    }
}
