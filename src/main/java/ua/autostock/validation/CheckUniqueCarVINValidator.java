package ua.autostock.validation;

import org.springframework.beans.factory.annotation.Autowired;
import ua.autostock.repository.CarRepository;
import ua.autostock.repository.UserRepository;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CheckUniqueCarVINValidator implements ConstraintValidator <CheckUniqueCarVIN, String>{

    @Autowired
    private CarRepository carRepository;

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (carRepository.findByVinIgnoreCase(value) == null){
            return true;
        }
        return false;
    }
}
