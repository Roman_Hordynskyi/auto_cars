package ua.autostock.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.autostock.dto.FilterDTO.MakeDTO;
import ua.autostock.repository.FilterRepo.MakeRepository;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class CheckPresentMakeForModelValidator implements ConstraintValidator<CheckPresentMakeForModel, MakeDTO> {

    @Autowired
    private MakeRepository makeRepository;

    @Override
    public boolean isValid(MakeDTO value, ConstraintValidatorContext context) {
        if(value.getId() == null) {
            return false;
        }
        if (makeRepository.findById(value.getId()).isPresent()) {
            return true;
        }
        return false;
    }
}
