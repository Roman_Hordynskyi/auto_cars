package ua.autostock.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD})
@Constraint(validatedBy = CheckPresentUserForCarValidator.class)
public @interface CheckPresentUserForCar {

    String message() default "user not found";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
