package ua.autostock.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD})
@Constraint(validatedBy = CheckPresentMakeForModelValidator.class)
public @interface CheckPresentMakeForModel {

    String message() default "user not found";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
