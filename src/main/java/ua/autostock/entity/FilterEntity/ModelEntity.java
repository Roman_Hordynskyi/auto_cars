package ua.autostock.entity.FilterEntity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor

@Entity
@Table(name = "model")
public class ModelEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "model", nullable = false, unique = true)
    private String model;

    @JsonIgnoreProperties("models")
    @ManyToOne
    @JoinColumn(name = "make_id", nullable = false)
    private MakeEntity make;

}
