package ua.autostock.entity.Enums;

import lombok.Getter;

@Getter
public enum GenderEnum {
    male, female;
}
