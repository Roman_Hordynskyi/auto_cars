package ua.autostock.entity.Enums;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public enum RoleEnum {
    USER, ADMIN;
}
