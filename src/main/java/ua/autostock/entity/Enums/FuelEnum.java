package ua.autostock.entity.Enums;

import lombok.Getter;

@Getter
public enum FuelEnum {
    diesel, gasoline, electricity, hybrid
}
