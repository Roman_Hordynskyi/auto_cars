package ua.autostock.dto.FilterDTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ua.autostock.validation.CheckUniqueFilterMake;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor

public class MakeDTO {

    private Long id;

    @NotBlank
    @CheckUniqueFilterMake
    private String make;

    @JsonIgnoreProperties("make")
    private List<ModelDTO> models;

}
