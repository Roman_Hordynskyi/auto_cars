package ua.autostock.dto.FilterDTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ua.autostock.validation.CheckPresentMakeForModel;
import ua.autostock.validation.CheckUniqueFilterModel;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@NoArgsConstructor

public class ModelDTO {

    private Long id;

    @NotBlank
    @CheckUniqueFilterModel
    private String model;

    @CheckPresentMakeForModel
    @JsonIgnoreProperties("models")
    private MakeDTO make;

}
