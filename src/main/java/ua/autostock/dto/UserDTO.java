package ua.autostock.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ua.autostock.entity.Enums.GenderEnum;
import ua.autostock.validation.CheckUniqueUserEmail;
import ua.autostock.validation.CheckUniqueTelephone;

import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class UserDTO extends BaseDTO{

    @NotNull(message = "First name can`t be NULL, you can use Ukr or Eng letters")
    @Size(min = 2, max = 20)
    @Pattern(regexp = "^[a-zA-ZА-ЩЬЮЯҐЄІЇа-щьюяґєії]+$")
    private String firstName;

    @NotNull(message = "Surname can`t be NULL, you can use Ukr or Eng letters")
    @Size(min = 2, max = 20)
    @Pattern(regexp = "^[a-zA-ZА-ЩЬЮЯҐЄІЇа-щьюяґєії]+$")
    private String surName;

    @NotNull(message = "telephone mush have 12 numbers, begin for 380")
    @CheckUniqueTelephone(message = "this telephone number already exists by another user")
    @Size(min = 12, max = 12)
    @Pattern(regexp = "^[0-9]+$")
    private BigDecimal telephone;

    @NotNull
    @CheckUniqueUserEmail(message = "this email already exists by another user")
    @Size(min = 10, max = 100)
    @Pattern(regexp = "^[a-z0-9_-]+@[a-z0-9.-]+\\.[a-z]{2,3}$", message = "bad format")
    private String email;

    @NotNull
    @Size(min = 8, max = 30)
    @Pattern(regexp = "^.*(?=.{8,})([a-zA-Z0-9]).*$", message = "password must have length must be more than 8")
    private String password;

    @NotNull
    @Size(min = 8, max = 30)
    @Pattern(regexp = "^.*(?=.{8,})([a-zA-Z0-9]).*$")
    private String passwordConfirm;

    @NotNull
    @Size
    private GenderEnum gender;

    @Min(value = 12)
    @Max(value = 90)
    @NotNull(message = "age must be from 12 to 90")
    private int age;

    private LocalDate registryDate;

}
