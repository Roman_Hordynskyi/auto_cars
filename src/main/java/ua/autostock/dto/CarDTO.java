package ua.autostock.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ua.autostock.entity.Enums.FuelEnum;
import ua.autostock.entity.PhotoEntity;
import ua.autostock.validation.CheckPresentUserForCar;
import ua.autostock.validation.CheckUniqueCarVIN;

import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class CarDTO extends BaseDTO {

	@Size(min = 2, max = 35)
	@Pattern(regexp = "^[a-zA-Z]+$")
	private String category;

	@Size(min = 2, max = 35)
	@Pattern(regexp = "^[a-zA-Z]+$")
	private String make;

	@Size(min = 1, max = 45)
	@Pattern(regexp = "^[a-zA-Z0-9]+$")
	private String model;

	@Min(value = 0)
	@Max(value = 999999)
	private int mileage;

	@NotNull
	private LocalDate dateOfManufacture;

	@NotNull
	private LocalDate addTime;

	@NotBlank
	@CheckUniqueCarVIN
	@Size(min = 17, max = 17)
	@Pattern(regexp = "^[a-zA-Z0-9]+$")
	private String vin;

	@NotNull
	@Min(value = 0)
	@Max(value = 9999999)
	private BigDecimal price;

	@NotBlank
	@Size(min = 2, max = 50)
	@Pattern(regexp = "^[a-zA-Z]+$")
	private String color;

	@NotNull
	@Min(value = 1, message = "min - 1")
	@Max(value = 99999, message = "max - 99 999")
	private BigDecimal volume;

	@NotBlank
	@Size(min = 1, max = 50)
	@Pattern(regexp = "^[a-zA-Z0-9]+$")
	private String drive;

	@NotBlank
	@Size(min=1, max = 50)
	@Pattern(regexp = "^[a-zA-Z0-9]+$")
	private String transmission;

	@NotBlank
	@Size(min = 2, max = 50)
	@Pattern(regexp = "^[a-zA-Z]+$")
	private String body;

	@NotBlank
	@Size(min = 2, max = 20)
	@Pattern(regexp = "^[a-zA-Z]+$")
	private FuelEnum fuel;

	//не працює на фронті валідація
	@NotNull
	@Min(value = 2)
	@Max(value = 5)
	private int numberOfDoors;

	@NotNull
	@Min(value = 2)
	@Max(value = 50)
	private int numberOfSeats;

	private String comfort;

	private String additionalEquipment;

	@NotBlank
	private String description;

	@JsonIgnoreProperties("favouriteCars")
	@CheckPresentUserForCar
	private UserDTO user;

	private List<PhotoEntity> photos;

}
