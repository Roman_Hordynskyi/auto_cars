package ua.autostock.dto;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class RoleDTO {

    private Long id;

    private String role;

}
