package ua.autostock.dto.Exception;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class ValidationError {

    private String field;
    private String message;

}
