package ua.autostock.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ua.autostock.entity.CarEntity;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class PhotoDTO  extends BaseDTO{

    @NotNull
    private String fileName;

    @NotNull
    private CarEntity car;

}
