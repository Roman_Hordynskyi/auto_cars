package ua.autostock.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ua.autostock.dto.CarDTO;
import ua.autostock.service.CarService;
import ua.autostock.service.FileStorageService;

import javax.validation.Valid;

@RestController
@RequestMapping("/cars")
public class CarController {
    @Autowired
    private CarService carService;

    @Autowired
    private FileStorageService storageService;

    @PostMapping
    public ResponseEntity<?> createCar(@Valid
                                       @RequestBody CarDTO car,
                                       @RequestParam("file") MultipartFile file){
            carService.saveCar(car);
    return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getCarById(@PathVariable Long id){
        return new ResponseEntity<>(carService.getCarById(id), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateCarById(@PathVariable Long id,
                                           @RequestBody CarDTO carEntity){
        return new ResponseEntity<>(carService.updateCar(carEntity), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<?> getCarsByPage(@PageableDefault Pageable pageable,
                                           @RequestParam(required = false) String make,
                                           @RequestParam(required = false) String model,
                                           @RequestParam(required = false) String vin){
        return new ResponseEntity<>(carService.getCarsByPageAndSearch(make, model, vin, pageable), HttpStatus.OK);
    }

//    @PostMapping("/image")
//    public ResponseEntity<?> handleFileUpload(@RequestParam("file") MultipartFile file,
//                                   @RequestParam("carId") Long id,
//                                   RedirectAttributes redirectAttributes) {
//        storageService.store(file);
//        return new ResponseEntity<>(HttpStatus.OK);
//    }

}
