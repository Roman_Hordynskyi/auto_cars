package ua.autostock.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.autostock.dto.UserDTO;
import ua.autostock.service.UserService;

import javax.validation.Valid;
import javax.validation.constraints.Min;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/{id}")
    public ResponseEntity<?> getUserById(
            @PathVariable Long id){
        return new ResponseEntity<>(userService.findUserById(id), HttpStatus.OK);
    }

    @GetMapping("/page")
    private ResponseEntity<?> getUsersByPage(@PageableDefault Pageable pageable){
        return  new ResponseEntity<>(userService.getUsersByPage(pageable), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> createUser(@Valid @RequestBody UserDTO user){
            userService.saveUser(user);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateUser(@Valid
                                        @RequestBody UserDTO userDto){
        return new ResponseEntity<>(userService.updateUserById(userDto), HttpStatus.OK);
    }

}
