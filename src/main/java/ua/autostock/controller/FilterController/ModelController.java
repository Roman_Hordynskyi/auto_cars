package ua.autostock.controller.FilterController;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.autostock.dto.FilterDTO.MakeDTO;
import ua.autostock.dto.FilterDTO.ModelDTO;
import ua.autostock.service.FilterServiseAndIMPL.ModelService;

import javax.validation.Valid;

@RestController
@RequestMapping("/filters/model")
public class ModelController {

    @Autowired
    private ModelService modelService;

    @PostMapping
    public ResponseEntity<?> addNewModel(@Valid @RequestBody ModelDTO modelDTO){
        modelService.addNewModel(modelDTO);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<?> getAllModels(){
        return new ResponseEntity<>(modelService.getAllModels(), HttpStatus.OK);
    }

    @GetMapping("/byMake")
    public ResponseEntity<?> getAllModelsByMake(@RequestBody MakeDTO makeDTO){
        return new ResponseEntity<>(modelService.getAllModelsByMake(makeDTO),HttpStatus.OK);
    }

    @DeleteMapping
    public ResponseEntity<?> deleteModel(@RequestParam(value = "id") Long id){
        modelService.removeModelById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
