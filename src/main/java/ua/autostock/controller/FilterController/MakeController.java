package ua.autostock.controller.FilterController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.autostock.dto.FilterDTO.MakeDTO;
import ua.autostock.service.FilterServiseAndIMPL.MakeService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/filters/make")
public class MakeController {
    @Autowired
    private MakeService makeService;

    @PostMapping
    public ResponseEntity<?> createNewMake(@Valid @RequestBody MakeDTO makeDTO){
        makeService.addNewMake(makeDTO);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<?> getAllMakes(){
        List<MakeDTO> makeDTOS = makeService.getAllMakes();
        return new ResponseEntity<>(makeDTOS, HttpStatus.OK);
    }

    @GetMapping("{makeId}")
    public ResponseEntity<?> getMakeById(){
        List<MakeDTO> makeDTOS = makeService.getAllMakes();
        return new ResponseEntity<>(makeDTOS, HttpStatus.OK);
    }

    @DeleteMapping("{makeId}")
    public ResponseEntity<?> deleteMake(@PathVariable("makeId") Long id){
        makeService.removeMakeById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
