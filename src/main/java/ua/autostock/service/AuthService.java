package ua.autostock.service;

import ua.autostock.dto.Request.SignInRequest;
import ua.autostock.dto.Request.SignUpRequest;

public interface AuthService {

    void registerUser(SignUpRequest request);

    String loginUser (SignInRequest request);

}
