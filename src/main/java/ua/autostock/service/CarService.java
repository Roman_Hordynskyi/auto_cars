package ua.autostock.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ua.autostock.dto.CarDTO;
import ua.autostock.entity.CarEntity;

public interface CarService {
    void saveCar(CarDTO car);

    CarEntity getCarById(Long id);

    CarEntity updateCar(CarDTO carToUpdate);

    boolean deleteCarById(Long id);

    Page<CarEntity> getCarsByPageAndSearch (String make,String model, String vin, Pageable pageable);

}
