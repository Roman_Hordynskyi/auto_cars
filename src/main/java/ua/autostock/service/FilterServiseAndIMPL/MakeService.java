package ua.autostock.service.FilterServiseAndIMPL;

import ua.autostock.dto.FilterDTO.MakeDTO;

import java.util.List;

public interface MakeService {

    void addNewMake(MakeDTO makeDTO);

    void removeMakeById(Long id);

    List<MakeDTO> getAllMakes();

    MakeDTO getMakeById(Long id);

}
