package ua.autostock.service.FilterServiseAndIMPL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.autostock.dto.FilterDTO.MakeDTO;
import ua.autostock.entity.FilterEntity.MakeEntity;
import ua.autostock.exceptions.NotFoundException;
import ua.autostock.repository.FilterRepo.MakeRepository;
import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MakeServiceIMPL implements MakeService {
    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private MakeRepository makeRepository;

    @Override
    public void addNewMake(MakeDTO makeDTO)  {
        makeRepository.save(modelMapper.map(makeDTO, MakeEntity.class));
    }

    @Override
    public void removeMakeById(Long id) {
        makeRepository.deleteById(id);
    }

    @Override
    public List<MakeDTO> getAllMakes() {
        return makeRepository.findAll().stream().map((x) -> modelMapper.map(x, MakeDTO.class)).collect(Collectors.toList());
    }

    @Override
    public MakeDTO getMakeById(Long id) {
        return modelMapper.map(makeRepository.findById(id).orElseThrow(()-> new NotFoundException("make not found")), MakeDTO.class);
    }
}
