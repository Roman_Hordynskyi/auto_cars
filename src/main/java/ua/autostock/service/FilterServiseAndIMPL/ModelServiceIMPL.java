package ua.autostock.service.FilterServiseAndIMPL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.autostock.dto.FilterDTO.MakeDTO;
import ua.autostock.dto.FilterDTO.ModelDTO;
import ua.autostock.entity.FilterEntity.MakeEntity;
import ua.autostock.entity.FilterEntity.ModelEntity;
import ua.autostock.repository.FilterRepo.ModelRepository;
import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ModelServiceIMPL implements ModelService {

    @Autowired
    private ModelRepository modelRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public void addNewModel(ModelDTO modelDTO) {
        modelRepository.save(modelMapper.map(modelDTO, ModelEntity.class));
    }

    @Override
    public List<ModelDTO> getAllModels() {
        List<ModelDTO> list = modelRepository.findAll().stream().map((x) -> modelMapper.map(x, ModelDTO.class)).collect(Collectors.toList());
        list.sort((p1, p2)-> p1.getModel().compareToIgnoreCase(p2.getModel()));
        return list;
    }

    @Override
    public List<ModelEntity> getAllModelsByMake(MakeDTO makeDTO) {
        return modelRepository.findAllByMake(modelMapper.map(makeDTO, MakeEntity.class));
    }

    @Override
    public void removeModelById(Long id) {
        modelRepository.deleteById(id);
    }

}
