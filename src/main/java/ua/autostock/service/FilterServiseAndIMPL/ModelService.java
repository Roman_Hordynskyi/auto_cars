package ua.autostock.service.FilterServiseAndIMPL;

import ua.autostock.dto.FilterDTO.MakeDTO;
import ua.autostock.dto.FilterDTO.ModelDTO;
import ua.autostock.entity.FilterEntity.ModelEntity;

import java.util.List;

public interface ModelService {

    void addNewModel(ModelDTO modelDTO);

    List<ModelDTO> getAllModels();

    List<ModelEntity> getAllModelsByMake(MakeDTO makeDTO);

    void removeModelById(Long id);
}
