package ua.autostock.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.autostock.dto.UserDTO;
import ua.autostock.exceptions.NotFoundException;
import ua.autostock.entity.UserEntity;
import ua.autostock.exceptions.ServerException;
import ua.autostock.repository.UserRepository;
import ua.autostock.service.UserService;
import org.modelmapper.ModelMapper;

import java.time.LocalDate;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ModelMapper modelMapper;

    @Override
    public UserDTO findUserById(Long id) {
        UserEntity userEntity = userRepository.findById(id).orElseThrow(()-> new NotFoundException("user with id [" + "] not found"));
        return modelMapper.map(userEntity, UserDTO.class);
    }

    @Override
    public UserDTO saveUser(UserDTO userDto) {
        if (userDto.getPassword().equals(userDto.getPasswordConfirm())) {
                UserEntity userEntity = modelMapper.map(userDto, UserEntity.class);
                userEntity.setRegistryDate(LocalDate.now());
                userRepository.save(userEntity);
            return userDto;
        }else {
            throw new ServerException("Password not match");
        }
    }

    @Transactional
    @Override
    public UserEntity updateUserById(UserDTO userToUpdate) {
        UserEntity userFromDB = userRepository.findById(userToUpdate.getId()).orElseThrow(()-> new NotFoundException("user not found"));
        modelMapper.map(userToUpdate, userFromDB);
        return userFromDB;
    }

    @Override
    public Page<UserEntity> getUsersByPage(Pageable pageable) {
        return userRepository.findAll(pageable);
    }
}











