package ua.autostock.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.autostock.dto.CarDTO;
import ua.autostock.exceptions.NotFoundException;
import ua.autostock.specification.CarSpecification;
import ua.autostock.entity.CarEntity;
import ua.autostock.repository.CarRepository;
import ua.autostock.service.CarService;
import org.modelmapper.ModelMapper;

import java.time.LocalDate;

@Service
public class CarServiceImpl implements CarService {

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public void saveCar(CarDTO car) {
        car.setAddTime(LocalDate.now());
        carRepository.save(modelMapper.map(car, CarEntity.class));
    }

    @Override
    public CarEntity getCarById(Long id) {
        return carRepository.findById(id).orElseThrow(()-> new NotFoundException("car not found"));
    }

    @Transactional
    @Override
    public CarEntity updateCar(CarDTO editCarDTO) {
        CarEntity carFromDb = carRepository.findById(editCarDTO.getId())
                .orElseThrow(()-> new NotFoundException("car not found"));
        modelMapper.map(editCarDTO, carFromDb);
        return carFromDb;
    }

    @Transactional
    @Override
    public boolean deleteCarById(Long id) {
        if(carRepository.existsById(id)){
            carRepository.deleteById(id);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Page<CarEntity> getCarsByPageAndSearch(String make, String model, String vin, Pageable pageable) {
        return carRepository.findAll(new CarSpecification(make,model,vin),pageable);
    }
}
