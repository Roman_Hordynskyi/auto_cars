package ua.autostock.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ua.autostock.dto.UserDTO;
import ua.autostock.entity.UserEntity;

public interface UserService {

    UserDTO saveUser (UserDTO userDto);

    UserDTO findUserById(Long id);

    Page<UserEntity> getUsersByPage(Pageable pageable);

    UserEntity updateUserById(UserDTO userToUpdate);

}
