package ua.autostock.repository.FilterRepo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import ua.autostock.entity.FilterEntity.MakeEntity;
import ua.autostock.entity.FilterEntity.ModelEntity;

import java.util.List;

@Repository
public interface ModelRepository extends JpaRepository<ModelEntity, Long>, JpaSpecificationExecutor {

    List<ModelEntity> findAllByMake (MakeEntity makeEntity);

    ModelEntity findByModel(String model);

}
