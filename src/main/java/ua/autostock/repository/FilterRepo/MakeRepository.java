package ua.autostock.repository.FilterRepo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.autostock.entity.FilterEntity.MakeEntity;

@Repository
public interface MakeRepository extends JpaRepository<MakeEntity, Long> {

    void deleteByMake(MakeEntity makeEntity);

    MakeEntity findByMake(String make);

}
