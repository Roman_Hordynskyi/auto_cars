package ua.autostock.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.autostock.entity.UserEntity;

import java.math.BigDecimal;

@Repository
public interface UserRepository extends JpaRepository <UserEntity, Long> {

    boolean existsByEmailIgnoreCase(String email);

    boolean existsByTelephone(BigDecimal telephone);

    UserEntity findByEmailIgnoreCase(String email);

    UserEntity findByTelephone(String telephone);

}
