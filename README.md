
You will only need to install JDK 8. A Maven wrapper has been provided in the project that you can use to build the projects.

* Available build options:
  * ```./mvnw clean install``` - Build the entire project and prepare all artifacts

* [MySQL](https://dev.mysql.com/downloads/mysql/) - Please use latest.

You should create schema in you local MysQL server with name auto_cars.
In first time starting project please uncomment function run in ProjecAplicatin file(to put some date to DB).
After that please comment that code.

### Local DB setup

```bash
prompt> mysql -h 127.0.0.1 -u root -p root
mysql> CREATE DATABASE auto_cars
```
